<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {


    $pagesize = 1;
    $data = array();

    for ($i = 1; $i <= $pagesize; $i++) {

          $x=$i + 1;


        $url = "https://www.italki.com/api/teachersv2?_r=1514591608590&country=&hl=de&is_instant=&is_native=&is_trial=&is_video=&page=$x&price_usd=&sort_type=5&speak=&teach=german&teacher_type=0";
//  Initiate curl
        $ch = curl_init();
// Disable SSL verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
        curl_setopt($ch, CURLOPT_URL, $url);
// Execute
        $result = curl_exec($ch);
// Closing
        curl_close($ch);

// Will dump a beauty json :3
        $json = json_decode($result, true);

        foreach ($json['data'] as $key => $tutor) {
            $t = new \App\Tutor();
            $t->textid = $tutor['textid'];
            $t->nickname = $tutor['nickname'];
            $t->page = $i;
            $t->rang = $key;
            $t->data = json_encode($tutor);
            $t->save();
        //    print_r($tutor);
        }


        if (!empty($json['meta'])) {
            $pagesize = $json['meta']['page_size'];
        }
    }
return view('welcome');

});
